---
title: Committee Minutes
seo_title: Committee Minutes - Eclipse IoT
description: The meeting minutes of the Eclipse IoT Working Group's steering committee.
hide_sidebar: true
---

The meeting minutes of the Eclipse IoT Working Group's steering committee.

{{< eclipsefdn_meeting_minutes yearly_sections_enabled="true" >}}