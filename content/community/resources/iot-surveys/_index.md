---
title: "Surveys"
description: "Our white papers are great resources for anyone looking at understanding how open source can help build successful IoT solutions"
aliases:
    - /iot-developer-surveys/
weight: 2
---

Explore the results of the influential annual IoT Developer Surveys on key IoT industry technologies, trends, and opportunities.

{{< newsroom/resources wg="eclipse_iot" type="survey_report" template="image-with-title" class="col-md-8" >}}
