---
title: "Case Studies"
description: "Useful resources to learn more about Eclipse IoT"
aliases:
    - /case-studies/
weight: 3
layout: "single"
---

{{< newsroom/resources wg="eclipse_iot" type="case_study" template="image-with-title" class="col-md-8" >}}
