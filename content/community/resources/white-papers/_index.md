---
title: "White Papers"
description: "Useful resources to learn more about Eclipse IoT"
aliases:
    - /white-papers/
icon: "file-text"
weight: 4
layout: "single"
---

{{< newsroom/resources wg="eclipse_iot" type="white_paper" template="image-with-title" class="col-md-8" >}}
