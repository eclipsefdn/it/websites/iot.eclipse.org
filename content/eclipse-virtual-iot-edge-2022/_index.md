---
title: "Virtual IoT & Edge Days 2022"
description: "Showcasing Innovation in IoT and Edge Computing"
headline: "Virtual IoT and <br> Edge Days 2022"
tagline: "Showcasing Innovation in IoT and Edge Computing <br> <span>Virtual Conference | May 24 - 25, 2022<span>"
date: 2022-02-05T16:09:45-04:00
hide_page_title: true
hide_sidebar: true
hide_breadcrumb: true
container: container-fluid
layout: single
cascade:
  hide_sidebar: true
  header_wrapper_class: "iot-day-2022"
  jumbotron_class: col-md-24 jumbotron-content
  page_css_file: /assets/css/eclipse-virtual-iot-edge-2022.min.css
---

{{< grid/section-container id="registration" class="padding-top-10 padding-bottom-60 text-center">}}
  {{< events/registration title="Thank You!" event="eclipse-virtual-iot-edge-2022">}}
Thanks to everyone who helped make Virtual IoT & Edge Days 2022 such a success, especially our speakers, program committee, and member companies.

[Watch the talk recordings](https://www.youtube.com/playlist?list=PLy7t4z5SYNaQejP4OMb-i3hC7-fO_u03j) on the Eclipse Foundation YouTube channel.
  {{</ events/registration >}}
{{</ grid/section-container >}}


{{< grid/section-container id="speakers" class="row text-center padding-top-20 padding-bottom-20">}}
{{< events/user_display event="eclipse-virtual-iot-edge-2022" title="Speakers" source="speakers" imageRoot="/assets/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}

{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-lighter-bg agenda-2022">}}
{{< grid/div class="padding-bottom-20" isMarkdown="false" >}}
{{< events/agenda event="eclipse-virtual-iot-edge-2022" >}}
{{</ grid/div >}}
{{</ grid/section-container >}}

 <!-- NOTE: hosted by and sponsors sections should always be the last ones on the page -->
{{< grid/section-container id="hosted-by" class="padding-top-20 padding-bottom-20" >}}
  <h2 class="text-center">Hosted by</h2>
  <p class="text-center">IoT & Edge Days 2022 is hosted by the Eclipse IoT, Edge Native, and Sparkplug working groups at the Eclipse Foundation.</p>
  {{< events/sponsors headerClass="hide" source="hostedBy" event="eclipse-virtual-iot-edge-2022" useMax="false" displayBecomeSponsor="false" >}}

{{</ grid/section-container >}}

{{< grid/section-container id="sponsors" class="white-row padding-top-20 padding-bottom-20 text-center" >}}
  {{< events/sponsors event="eclipse-virtual-iot-edge-2022" title="Thanks to Our Participating Member Companies" useMax="false" displayBecomeSponsor="false" >}}
{{</ grid/section-container >}}

{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}
