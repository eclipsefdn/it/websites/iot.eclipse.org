---
title: Call for Proposals
headline: Call for Proposals
description: ''
header_wrapper_class: iot-day-2023 sub-page
container: container
---

{{< grid/div class="alert alert-info" isMarkdown="false" >}}
    The call for proposals is now over.
{{</ grid/div >}}

Virtual IoT & Edge Days 2023 brings together adopters, developers, technology enthusiasts and
market leaders to collaborate virtually. Engage with the leaders of the IoT, Edge Native,
Sparkplug and Oniro working groups as we change the world - one device at a time.

## Important Dates

- CFP opens: Tuesday, February 15, 2023
- CFP closes: 11:59pm Pacific Daylight Time (PDT), Thursday, March 31, 2023
- Program announced: Tuesday, April 15, 2023
- Event Dates: Tuesday, May 16 – Wednesday, May 17, 2023

## First Time Submitting? Don't Feel Intimidated

IoT & Edge Days are an excellent way to get to know the community and share your ideas and the work that you are doing. You do not need to be a chief architect or long-time industry pundit to submit a proposal; in fact, we strongly encourage first-time speakers to submit talks and engage with the Eclipse IoT and Edge community. We will continue to work with partners at the working groups and potential sponsors to help keep IoT & Edge days professional, welcoming, and friendly.

## How to Submit Your Proposal

We have done our best to make the submission process as simple as possible. Here is what you will need to do when filling out the submission form.

- Choose a submission format (NEW formats!)
  - Solo Session: 45 minutes - great for raising project awareness or getting feedback on a specific problem or topic
  - Panel Discussion: two or more speakers, 60 minutes (50 minutes presenting, 10 minutes for questions) - great for demonstrating a product, presenting slides, or discussing a hypothesis
- List your session title
- Provide a detailed and focused description with a maximum of 900 characters
- Complete the Speaker Agreement

If you are chosen to be a speaker, we will ask you to

- Send in a photo and bio
- Schedule a tech-check for your presentation equipment and environment

## Ready to Submit?

{{< grid/div isMarkdown="false" >}}
    <a class="btn btn-primary" href="https://forms.gle/sPWCSyz2RqPY9tYG7">Propose a Talk</a>
{{</ grid/div >}}

## Questions?

If you have any questions on how to submit a proposal or the event in general, please contact <events@eclipse.org>.
