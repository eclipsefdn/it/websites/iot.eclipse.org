---
title: Virtual IoT & Edge Days 2023
description: "Showcasing Innovation in IoT and Edge Computing"
date: 2023-05-16
headline: Virtual Iot & Edge Days 2023
subtitle: Showcasing Innovation in IoT and Edge Computing
tagline: Virtual Conference | May 16 - 17, 2023
hide_page_title: true
hide_breadcrumb: true
hide_sidebar: true
header_wrapper_class: iot-day-2023 
container: container-fluid
layout: single
cascade:
    hide_sidebar: true
    jumbotron_class: col-md-9 jumbotron-content
    jumbotron_tagline_class: col-sm-24 jumbotron-tagline
    page_css_file: /assets/css/eclipse-virtual-iot-edge-2023.min.css
---

<!-- Registration -->
{{< grid/section-container id="registration" class="padding-top-10 padding-bottom-60 text-center" isMarkdown="false" >}}
    {{< events/registration event="eclipse-virtual-iot-2023" >}}
Virtual IoT and Edge Days 2023 is an online event for IoT and Edge developers and thought leaders, with an emphasis on Eclipse-driven projects and technologies. The aim of the event is to gather our contributors and committers to share the latest news and trends. It is also an opportunity to reach out to the wider open source IoT and Edge community.

The event is open to anyone interested in IoT and Edge topics. It is hosted jointly by the Eclipse IoT, Edge Native, Sparkplug and Oniro working groups at the Eclipse Foundation.
    {{</ events/registration >}}
{{</ grid/section-container >}}

<!-- Speakers -->
{{< grid/section-container id="speakers" class="row text-center padding-top-20 padding-bottom-20">}}
    {{< events/user_display event="eclipse-virtual-iot-2023" title="Speakers" source="speakers" imageRoot="./images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}

<!-- Agenda -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-lighter-bg">}}
    {{< grid/div class="padding-bottom-20" isMarkdown="false" >}}
        {{< events/agenda event="eclipse-virtual-iot-2023" >}}
    {{</ grid/div >}}
{{</ grid/section-container >}}

{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}

<!-- Hosted by -->
{{< grid/section-container id="hosted-by" class="padding-top-60 padding-bottom-60 text-center" >}}
  <h2>Hosted by</h2>
  <p>
    IoT & Edge Days 2023 is hosted by the Eclipse IoT Working Group, the Eclipse Edge Native 
    Working Group, the Eclipse Sparkplug Working Group and the Oniro Working Group.
  </p>
  {{< events/sponsors headerClass="hide" source="hosted_by" event="eclipse-virtual-iot-2023" useMax="false" displayBecomeSponsor="false" >}}

{{</ grid/section-container >}}

<!-- Sponsors -->
{{< grid/section-container id="sponsors" class="white-row padding-top-20 padding-bottom-20 text-center" >}}
  {{< events/sponsors event="eclipse-virtual-iot-2023" title="Thanks to Our Participating Member Companies" useMax="false" displayBecomeSponsor="false" >}}
{{</ grid/section-container >}}
