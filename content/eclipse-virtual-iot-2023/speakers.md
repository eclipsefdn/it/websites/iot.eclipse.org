---
title: Speakers 
headline: Speakers 
description: Speakers for the Eclipse Virtual IoT & Edge Days 2023
header_wrapper_class: iot-day-2023 sub-page
container: container
---

{{< events/user_bios event="eclipse-virtual-iot-2023" source="speakers" imgRoot="../images/speakers/" >}}
